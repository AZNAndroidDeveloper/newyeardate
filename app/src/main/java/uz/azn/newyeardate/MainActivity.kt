package uz.azn.newyeardate

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import uz.azn.newyeardate.databinding.ActivityMainBinding
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.Month
import java.util.*


class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    @SuppressLint("SimpleDateFormat")
    val dateFormat = SimpleDateFormat("dd/M/yyyy hh:mm:ss")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val neaw_year = LocalDateTime.of(2020,Month.JANUARY,31,23,59,59)
      val countDownTimer =   object : CountDownTimer(Long.MAX_VALUE,1000) {
            override fun onFinish() {
                TODO("Not yet implemented")
            }

            override fun onTick(millisUntilFinished: Long) {
            val current  = LocalDateTime.now()
                neaw_year.month
                Log.d("User",neaw_year.minusYears(2030).toString() )
                val day = neaw_year.dayOfMonth-current.dayOfMonth
                val hours = neaw_year.hour - current.hour
                val minut = neaw_year.minute - current.minute
                val second = neaw_year.second - current.second
                binding.dayNumber.text = day.toString()
                binding.hourNumber.text = hours.toString()
                binding.minutesNumber.text = minut.toString()
                binding.secondNumber.text = second.toString()
            }
        }
        countDownTimer.start()


    }
}
